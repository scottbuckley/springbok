package docstyle

import org.bitbucket.springbok.DocRatsParserSyntax._

sealed abstract class ElemSel {
  def matches(n:Elem): Boolean

  def matches(n:Element):Boolean = {
    n match {
      case e:Elem => matches(e)
      case _      => false
    }
  }
}

case class TagSel(tag: String) extends ElemSel {
  def matches(n: Elem): Boolean = (n.tag == tag)
}

case class IDSel(id: String) extends ElemSel {
  def matches(n:Elem): Boolean = {
    n.optPropDefs collectFirst {
      case PropIDDef(str) if (str==id) => str
    } match {
      case Some(s) => true
      case _       => false
    }
  }
}

case class ClassSel(cl: String) extends ElemSel{
  def matches(n:Elem): Boolean = {
    n.optPropDefs collectFirst {
      case PropKlassDef(str) if (str==cl) => str
    } match {
      case Some(s) => true
      case _       => false
    }
  }
}

case class StyleRule(sel: ElemSel, defs: Vector[PropDef])

object style {
  def getDefaultStylesheet(): Vector[StyleRule] = {
    Vector(
        StyleRule(TagSel("html"),        Vector( PropDisplayDef( DisplayBlock() )))
      , StyleRule(TagSel("address"),     Vector( PropDisplayDef( DisplayBlock() )))
      , StyleRule(TagSel("blockquote"),  Vector( PropDisplayDef( DisplayBlock() )))
      , StyleRule(TagSel("body"),        Vector( PropDisplayDef( DisplayBlock() )))
      , StyleRule(TagSel("dd"),          Vector( PropDisplayDef( DisplayBlock() )))
      , StyleRule(TagSel("div"),         Vector( PropDisplayDef( DisplayBlock() )))
      , StyleRule(TagSel("dl"),          Vector( PropDisplayDef( DisplayBlock() )))
      , StyleRule(TagSel("dt"),          Vector( PropDisplayDef( DisplayBlock() )))
      , StyleRule(TagSel("fieldset"),    Vector( PropDisplayDef( DisplayBlock() )))
      , StyleRule(TagSel("form"),        Vector( PropDisplayDef( DisplayBlock() )))
      , StyleRule(TagSel("frame"),       Vector( PropDisplayDef( DisplayBlock() )))
      , StyleRule(TagSel("frameset"),    Vector( PropDisplayDef( DisplayBlock() )))
      , StyleRule(TagSel("h1"),          Vector( PropDisplayDef( DisplayBlock() )))
      , StyleRule(TagSel("h2"),          Vector( PropDisplayDef( DisplayBlock() )))
      , StyleRule(TagSel("h3"),          Vector( PropDisplayDef( DisplayBlock() )))
      , StyleRule(TagSel("h4"),          Vector( PropDisplayDef( DisplayBlock() )))
      , StyleRule(TagSel("h5"),          Vector( PropDisplayDef( DisplayBlock() )))
      , StyleRule(TagSel("h6"),          Vector( PropDisplayDef( DisplayBlock() )))
      , StyleRule(TagSel("noframes"),    Vector( PropDisplayDef( DisplayBlock() )))
      , StyleRule(TagSel("ol"),          Vector( PropDisplayDef( DisplayBlock() )))
      , StyleRule(TagSel("p"),           Vector( PropDisplayDef( DisplayBlock() )))
      , StyleRule(TagSel("center"),      Vector( PropDisplayDef( DisplayBlock() )))
      , StyleRule(TagSel("dir"),         Vector( PropDisplayDef( DisplayBlock() )))
      , StyleRule(TagSel("hr"),          Vector( PropDisplayDef( DisplayBlock() )))
      , StyleRule(TagSel("menu"),        Vector( PropDisplayDef( DisplayBlock() )))
      , StyleRule(TagSel("pre"),         Vector( PropDisplayDef( DisplayBlock() )))
      , StyleRule(TagSel("li"),          Vector( PropDisplayDef( DisplayListItem() )))
      , StyleRule(TagSel("head"),        Vector( PropDisplayDef( DisplayNone() )))
      , StyleRule(TagSel("table"),       Vector( PropDisplayDef( DisplayTable() )))
      , StyleRule(TagSel("tr"),          Vector( PropDisplayDef( DisplayTableRow() )))
      , StyleRule(TagSel("thead"),       Vector( PropDisplayDef( DisplayTableHeaderGroup() )))
      , StyleRule(TagSel("tbody"),       Vector( PropDisplayDef( DisplayTableRowGroup() )))
      , StyleRule(TagSel("tfoot"),       Vector( PropDisplayDef( DisplayTableFooterGroup() )))
      , StyleRule(TagSel("col"),         Vector( PropDisplayDef( DisplayTableColumn() )))
      , StyleRule(TagSel("colgroup"),    Vector( PropDisplayDef( DisplayTableColumnGroup() )))
      , StyleRule(TagSel("td"),          Vector( PropDisplayDef( DisplayTableCell() )))
      , StyleRule(TagSel("th"),          Vector( PropDisplayDef( DisplayTableCell() )))
      , StyleRule(TagSel("caption"),     Vector( PropDisplayDef( DisplayTableCaption() )))
      , StyleRule(TagSel("input"),       Vector( PropDisplayDef( DisplayInlineBlock() )))
      , StyleRule(TagSel("select"),      Vector( PropDisplayDef( DisplayInlineBlock() )))
      


      , StyleRule(TagSel("body"),  Vector( PropMarginLeftDef(   MetricInt(8) )))
      , StyleRule(TagSel("body"),  Vector( PropMarginRightDef(  MetricInt(8) )))
      , StyleRule(TagSel("body"),  Vector( PropMarginTopDef(    MetricInt(8) )))
      , StyleRule(TagSel("body"),  Vector( PropMarginBottomDef( MetricInt(8) )))


    )
  }
}