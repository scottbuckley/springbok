import render._
import docstyle._
import org.bitbucket.inkytonik.kiama.util.{ Position, StringSource, PositionStore, Messaging }
import org.bitbucket.inkytonik.kiama.parsing.{ Parsers, Success }
import org.bitbucket.springbok.DocRatsParser
import org.bitbucket.springbok.DocRatsParserSyntax._
import org.bitbucket.inkytonik.kiama.output.PrettyPrinter

// case class ElemDoc(root:Elem) extends Product
// abstract class ElemType(chren: Seq[Elem]) extends Product
// case class Elem(tag:String, chren:Seq[Elem], props:Props=new Props()) extends ElemType(chren, props)
// case class TextElem(str:String) extends ElemType(Vector())

object RatsParser extends PositionStore with Messaging {

    import org.bitbucket.springbok.{DocRatsParserSyntax => s}

    def productpp(p : AnyRef) : String = 
      PrettyPrinter.layout(PrettyPrinter.any(p))

    def ratsparse(str: String):Doc = {
      val p = new DocRatsParser(StringSource(str), positions)
      val pr = p.pDoc(0)

      if (pr.hasValue) {
        val d = p.value(pr).asInstanceOf[Doc]
        println (productpp(d))
        d
      }
      else {
        println (formatMessage(p.errorToMessage(pr.parseError)))
        new Doc(Vector())
      }
    }
}


// An App has a Main object (which is the window), and
// a Doc, which is the document to be rendered. App will be
// extended by some app(s), which mostly define doc and
// getRenderer, which are passed to main and there used
// to render the document.
trait App extends PositionStore {

  // to be defined in implementations
  val doc: Element
  val getRenderer: Element => Renderer
  val docStylesheet: Vector[StyleRule] = Vector()
  val defStylesheet: Vector[StyleRule] = style.getDefaultStylesheet()

  // // for parsing documents
  // val parser = new Parsers(positions) with DocParser with StyleParser

  // create a "Main" object to host the document. this creates
  // the window and kicks off the rendering process.
  def main(args: Array[String]): Unit = {
    new Main(doc, getRenderer).main(args)
  }

  def parse_sbtrats(str: String): Element = {
    getFirstElem(RatsParser.ratsparse(str))
  }

  def getFirstElem(d:Doc): Element = {
    d.optElements.head
  }
}
