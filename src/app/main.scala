import java.awt.{ Graphics, Graphics2D }
import javax.swing.{ JPanel, JFrame }

import org.bitbucket.springbok.DocRatsParserSyntax._
import render._

// Doc: document to be rendered
// getRenderer: function returning used renderer
class Main(var doc: Element, getRenderer: Element => Renderer = (d) => new Renderer_K2_Floats(d)) extends JPanel {

  // default window dimensions.
  val wWidth  = 800;
  val wHeight = 240;

  // local main (to be called by App)
  def main(args: Array[String]): Unit = {

    // set up window
    val frame: JFrame = new JFrame("Springbok")
    this.setLayout(null)
    frame.add(this)
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setSize(wWidth, wHeight)
    frame.setLocationRelativeTo(null)
    frame.setVisible(true)
  }

  // render a document every "frame"
  override def paint(g: Graphics): Unit = {
    super.paint(g)
    val g2: Graphics2D = g.asInstanceOf[Graphics2D]

    // apply default stylesheet
    // val final_doc = DefaultStyleSheet.apply_stylesheet(doc) // stylesheets are moving to AGs

    // reinstantiates currentRenderer, which clears all attribute caches.
    val currentRenderer: Renderer = getRenderer(doc)

    // render document
    currentRenderer.init(g2, doc)
    currentRenderer.render(doc)
  }
}
