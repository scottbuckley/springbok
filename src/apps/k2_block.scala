import org.bitbucket.springbok.DocRatsParserSyntax._

object K2_Block extends App {

  override val getRenderer = (d:Element) => new render.Renderer_K2_Block(d)

  val doc = parse_sbtrats("""
    <div height=100>
        <div height=40, width=200>
            <div height=10></div>
            <div height=20></div>
        </div>
        <div height=50></div>
    </div>""")

}