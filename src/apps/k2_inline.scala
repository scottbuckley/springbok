import org.bitbucket.springbok.DocRatsParserSyntax._

object K2_Inline extends App {

  override val getRenderer = (d:Element) => new render.Renderer_K2_Inline(d)

  val doc = parse_sbtrats("""
    <div height=100>
        <span width=110, height=22></span>
        <b color=Blue>
        <span width=90, height=25></span>
        <span width=80, height=28></span>
        </b>
        <span width=75, height=31></span>
    </div>""")

}