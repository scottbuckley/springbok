import org.bitbucket.springbok.DocRatsParserSyntax._

object K2_Floats extends App {

  override val getRenderer = (d:Element) => new render.Renderer_K2_Floats(d)

  val docstring = """
    <div height=100>
        <div height=40></div>
        <div width=50, height=50></div>
        <div width=50, height=50></div>
        <div width=50, height=50></div><div width=50, height=50></div>
        <div width=50, height=50></div>
        <div width=50, height=50, float=Left></div>
        <div width=50, height=50></div>
        <div width=50, height=50></div>
        <div width=50, height=50></div>
    </div>"""

    val doc = parse_sbtrats(docstring)

}