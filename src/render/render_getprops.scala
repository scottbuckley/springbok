package render

import org.bitbucket.springbok.DocRatsParserSyntax._
import docstyle._

// ---- sealed abstract class PropDef extends ASTNode ---- //
//  case class PropIDDef (propString : PropString) extends PropDef  
//  case class PropKlassDef (propString : PropString) extends PropDef  
//  case class PropHrefDef (propString : PropString) extends PropDef  
//  case class PropSrcDef (propString : PropString) extends PropDef  
//  case class PropFontDef (propString : PropString) extends PropDef  
//  case class PropFontStyleDef (propFontStyle : PropFontStyle) extends PropDef  
//  case class PropFontSizeDef (propMetric : PropMetric) extends PropDef  
//  case class PropWidthDef (propAutoMetric : PropAutoMetric) extends PropDef  
//  case class PropHeightDef (propAutoMetric : PropAutoMetric) extends PropDef  
//  case class PropPaddingTopDef (propMetric : PropMetric) extends PropDef  
//  case class PropPaddingLeftDef (propMetric : PropMetric) extends PropDef  
//  case class PropPaddingRightDef (propMetric : PropMetric) extends PropDef  
//  case class PropPaddingBottomDef (propMetric : PropMetric) extends PropDef  
//  case class PropMarginTopDef (propMetric : PropMetric) extends PropDef  
//  case class PropMarginLeftDef (propMetric : PropMetric) extends PropDef  
//  case class PropMarginRightDef (propMetric : PropMetric) extends PropDef  
//  case class PropMarginBottomDef (propMetric : PropMetric) extends PropDef  
//  case class PropBorderTopDef (propMetric : PropMetric) extends PropDef  
//  case class PropBorderLeftDef (propMetric : PropMetric) extends PropDef  
//  case class PropBorderRightDef (propMetric : PropMetric) extends PropDef  
//  case class PropBorderBottomDef (propMetric : PropMetric) extends PropDef  
//  case class PropDisplayDef (propDisplay : PropDisplay) extends PropDef  
//  case class PropPositionDef (propPosition : PropPosition) extends PropDef  
//  case class PropFloatDef (propFloat : PropFloat) extends PropDef  

object TextElementProps {
  def propDefsForTextElement():Vector[PropDef] = {
    // this defines what i get if i ask for the propdef of a text element
    Vector(new PropDisplayDef(new DisplayInline()))
  }
}

abstract class PropRenderer(doc:Element) extends ColoredRenderer(doc) {
    
  case class PropMatcher(pred:PropDef=>Boolean, inherited: Boolean, initial: PropDef) {

    // default stylesheet
    val defSS = style.getDefaultStylesheet()


    object ContainsDef {
      def unapply(pds:Vector[PropDef]): Option[PropDef] = {
        pds find pred
      }
    }

    object DefLocal {
      def unapply(n:Element): Option[PropDef] = {
        ContainsDef.unapply(elemDefs(n))
      }
    }

    object DefFromDefSS {
      def unapply(n:Element) : Option[PropDef] = {
        defSS collectFirst {
          case StyleRule(sel, ContainsDef(d)) if sel.matches(n) => d
        }
      }
    }

    val propAttr: Element => PropDef = attr {
      // case _:AnonElem                  => initial

      // was it defined on the doc directly?
      case DefLocal(f)                 => f

      // if not, is it in the default stylesheet?
      case DefFromDefSS(f)             => f

      // if not, either inherit or use the default value
      case tree.parent(p) if inherited => propAttr(p)
      case _                           => initial
    }

  }





  val elemDefs: Element => Vector[PropDef] = attr {
    case Elem(_, p, _) => p
    case _:TextElem    => TextElementProps.propDefsForTextElement()
    case _             => Vector()
  }



  // WIDTH
  val widthMatcher = new PropMatcher(
      { case _:PropWidthDef => true ; case _ => false }
    , false
    , new PropWidthDef(MetricAuto())
  )
  val propWidthDef: Element => PropDef = widthMatcher.propAttr

  // b just adds a "false" default to a partial boolean function
  def b(f: PartialFunction[PropDef, Boolean]): Function[PropDef,Boolean] = { arg => f.applyOrElse(arg, (_:PropDef)=>false) }

  // HEIGHT
  val heightMatcher = new PropMatcher(
      b{ case _:PropHeightDef => true }
    , false
    , new PropHeightDef(MetricAuto())
  )
  val propHeightDef: Element => PropDef = heightMatcher.propAttr

  // POSITION
  val positionMatcher = new PropMatcher(
      b{ case _:PropPositionDef => true }
    , false
    , new PropPositionDef(PositionStatic())
  )
  val propPositionDef: Element => PropDef = positionMatcher.propAttr

  // POSITION
  val colorMatcher = new PropMatcher(
      b{ case _:PropColorDef => true }
    , true
    , new PropColorDef(ColorGreen())
  )
  val propColorDef: Element => PropDef = colorMatcher.propAttr 

  // FLOAT
  val floatMatcher = new PropMatcher(
      b{ case _:PropFloatDef => true }
    , false
    , new PropFloatDef(FloatLeft())
  )
  val propFloatDef: Element => PropDef = floatMatcher.propAttr

  // DISPLAY
  val displayMatcher = new PropMatcher(
      b{ case _:PropDisplayDef => true }
    , false
    , new PropDisplayDef(DisplayInline())
  )
  val propDisplayDef: Element => PropDef = displayMatcher.propAttr
  



  //            _                  _
  //   _____  _| |_ _ __ __ _  ___| |_ ___  _ __ ___
  //  / _ \ \/ / __| '__/ _` |/ __| __/ _ \| '__/ __|
  // |  __/>  <| |_| | | (_| | (__| || (_) | |  \__ \
  //  \___/_/\_\\__|_|  \__,_|\___|\__\___/|_|  |___/

  def propAttrTypeErr(propname:String):Exception = {
    new Exception(s"prop${propname}Def returned a PropDef that isn't a Prop${propname}Def. This should never happen!")
  }

  // some extractors. helpers mostly for checking style properties within pattern matches
  object Width {
    def unapply(n: Element): Option[Double] =
      propWidthDef(n) match {
        case PropWidthDef(MetricInt(w))  => Some(w)
        case _                           => None
      }
  }

  object Height {
    def unapply(n: Element): Option[Double] =
      propHeightDef(n) match {
        case PropHeightDef(MetricInt(h)) => Some(h)
        case _                           => None
      }
  }
  object Float {
    def unapply(n: Element): Option[PropFloat] = {
      propFloatDef(n) match {
        case PropFloatDef(FloatNone()) => None
        case PropFloatDef(f)           => Some(f)
        case _                         => throw propAttrTypeErr("Float")
      }
    }
  }
  object Position {
    def unapply(n: Element): Option[PropPosition] =
      propPositionDef(n) match {
        case PropPositionDef(p) => Some(p)
        case _                  => throw propAttrTypeErr("Position")
      }
  }
  object Display {
    def unapply(n: Element): Option[PropDisplay] = {
      propDisplayDef(n) match {
        case PropDisplayDef(DisplayNone()) => None
        case PropDisplayDef(d)             => Some(d)
        case _                             => throw propAttrTypeErr("Display")
      }
    }
  }


}









