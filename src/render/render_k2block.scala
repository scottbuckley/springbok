package render

import java.awt.{ Color, Graphics, Graphics2D, Font, BasicStroke, Rectangle }
import java.awt.geom.Line2D
import java.awt.font._
import java.text._
import javax.swing._
import org.bitbucket.inkytonik.kiama.attribution._
import org.bitbucket.inkytonik.kiama.attribution.Attribution
import org.bitbucket.inkytonik.kiama.rewriting.{ Rewriter, Strategy }
import scala.collection.generic.CanBuildFrom
import scala.language.higherKinds
import scala.collection.mutable.Set
import org.bitbucket.inkytonik.dsprofile.Events.{ wrap }
import scala.math.{ min, max }

import org.bitbucket.springbok.DocRatsParserSyntax._

class Renderer_K2_Block(doc:Element) extends PropRenderer(doc) {

  object Both {
    def unapply(x: Any): Option[(Any, Any)] =
      Some((x, x))
  }

  object HasSpecifiedWidth {
    def unapply(n: Element): Option[Double] =
      propWidthDef(n) match {
        case PropWidthDef(MetricInt(w))  => Some(w)
        case _                           => None
      }
  }

  object HasSpecifiedHeight {
    def unapply(n: Element): Option[Double] =
      propHeightDef(n) match {
        case PropHeightDef(MetricInt(h)) => Some(h)
        case _                           => None
      }
  }




  

  val relx: Element => Double = attr {
    case _ => 0.0
  }

  val rely: Element => Double = attr {
    case tree.prev(prev) => rely(prev) + height(prev)
    case _ => 0.0
  }

  val width: Element => Double = attr {
    case Both(HasSpecifiedWidth(w), tree.parent(par)) => min(w, width(par))
    case HasSpecifiedWidth(w) => w
    case tree.parent(par) => width(par)
    case _ => windowWidth
  }

  val height: Element => Double = attr {
    case n @ HasSpecifiedHeight(h) => max(h, chren(n).map(height).sum)
    case n => chren(n).map(height).sum
  }

  // convert relative to absolute positions trivially
  val absy: Element => Double = attr {
    case n @ tree.parent(par) => absy(par) + rely(n)
    case n => rely(n)
  }
  val absx: Element => Double = attr {
    case n @ tree.parent(par) => absx(par) + relx(n)
    case n => relx(n)
  }

  val renderColor: Element => Color = attr {
    case HasSpecifiedWidth(_) => new Color(0.0f, 1.0f, 0.5f, 0.25f)
    case _ => new Color(0.5f, 0.25f, 0.5f, 0.25f)
  }

  // specify which attributes will be used for rendering
  val renderX = absx
  val renderY = absy
  val renderWidth = width
  val renderHeight = height

}