package render

import java.awt.{ Color, Graphics, Graphics2D, Font, BasicStroke, Rectangle }
import java.awt.geom.Line2D
import java.awt.font._
import java.text._
import javax.swing._
import org.bitbucket.inkytonik.kiama.attribution._
import org.bitbucket.inkytonik.kiama.attribution.Attribution
import org.bitbucket.inkytonik.kiama.rewriting.{ Rewriter, Strategy }
import scala.collection.generic.CanBuildFrom
import scala.language.higherKinds
import scala.collection.mutable.Set
import org.bitbucket.inkytonik.dsprofile.Events.{ wrap }
import scala.math.{ min, max }
import org.bitbucket.inkytonik.kiama.util.Comparison.distinct

import org.bitbucket.springbok.DocRatsParserSyntax._

class Renderer_K2_Inline(doc:Element) extends PropRenderer(doc) {
  
  lazy val decorators = new MyDecorators(tree)
  import decorators.{downOptPar, downPar, chain, down}

  def fproduct[A,B](first: A=>B, second: A=>B, product: (B,B)=>B): (A=>B) = 
    x => product(first(x), second(x))

  def sum(a:Double, b:Double):Double = a+b

  def fsum[A](first: A=>Double, second: A=>Double): (A=>Double) = 
    fproduct(first, second, sum)

  def max0(l: Seq[Double]): Double =
    if (l.isEmpty) 0 else l.max


  //            _                  _
  //   _____  _| |_ _ __ __ _  ___| |_ ___  _ __ ___
  //  / _ \ \/ / __| '__/ _` |/ __| __/ _ \| '__/ __|
  // |  __/>  <| |_| | | (_| | (__| || (_) | |  \__ \
  //  \___/_/\_\\__|_|  \__,_|\___|\__\___/|_|  |___/

  // allows you to match _ against two sub-patterns.
  object Both {
    def unapply(x: Any): Option[(Any, Any)] =
      Some((x, x))
  }

  // return _'s metric width, or None
  object HasSpecifiedWidth {
    def unapply(n: Element): Option[Double] =
      propWidthDef(n) match {
        case PropWidthDef(MetricInt(w))  => Some(w)
        case _                           => None
      }
  }

  // return _'s metric height, or None
  object HasSpecifiedHeight {
    def unapply(n: Element): Option[Double] =
      propHeightDef(n) match {
        case PropHeightDef(MetricInt(h)) => Some(h)
        case _                           => None
      }
  }

  // access isInlineLevel
  object Inline {
    def unapply(n:Element) : Boolean = 
      isInlineLevel(n)
  }

  // access isBlockLevel
  object Block {
    def unapply(n:Element) : Boolean = 
      isBlockLevel(n)
  }

  // access isInlineBlock
  object InlineBox {
    def unapply(n:Element) : Boolean = 
      isInlineBox(n)
  }


  //        _   _        _ _           _
  //   __ _| |_| |_ _ __(_) |__  _   _| |_ ___  ___
  //  / _` | __| __| '__| | '_ \| | | | __/ _ \/ __|
  // | (_| | |_| |_| |  | | |_) | |_| | ||  __/\__ \
  //  \__,_|\__|\__|_|  |_|_.__/ \__,_|\__\___||___/

  // AIS STUFF
  val lineBoxChain = chain[LineBox](
    (in  => {
      case n@tree.parent()     => new LineBox(containingBlock(n), None)
      case n if lineBoxHead(n) => new LineBox(containingBlock(n), Some(in(n)))
    })
  )

  val containedChren: Element => Vector[Element] = attr {
    case n if !isContainingBlock(n) => Vector()
    case n => chrenFlat(n).filter(containingBlock(_) eq n)
  }

  val lineBoxHead: Element => Boolean = attr {
    case InlineBox() => false
    case n@Inline() => prevInline(n) match {
      case None    => true
      case Some(p) => relx(p)+width(p)+width(n) > width(containingBlock(p))
    }
    case _ => false
  }



  //////   LINEBOX / PARENT ACCESS   //////

  // linebox => children
  val lineMembers: LineBox => Vector[Element] = attr {
    case n => containedChren(n.element)
              .filter(lineBox(_) eq n)
              .filter(isLineMember)
  }

  // linebox => parent
  val lineOwner: LineBox => Element = attr {
    case n => n.element
  }
  
  // parent => lineboxes
  val lineBoxes: Element => Vector[LineBox] = attr {
    case n => distinct(containedChren(n).map(lineBox))
  }

  // child => linebox
  val lineBox: Element => LineBox = 
    lineBoxChain.in
  
  override val alsoRender: Element => Vector[Element] = attr {
    case _:AnonElem => Vector()
    case n => lineBoxes(n).map(a => AnonElem(a))
  }
  
  
  
  // END AIS STUFF

  val isInlineBox: Element => Boolean = attr {
    //hacked for simplicity
    case Elem("b", _, _) => true
    case _               => false
  }

  val isLineMember: Element => Boolean = attr {
    case InlineBox() => false
    case Inline()    => true
    case _           => false
  }

  val isContainingBlock: Element => Boolean = attr {
    //hacked for simplicity
    case InlineBox() => false
    case Block() => true
    case _       => false
  }

  val containingBlock: Element => Element =
  down[Element]((_:Element) => tree.root) {
    case n if isContainingBlock(n) => n
  }

  val prevInlineChain = chain[Map[Element,Element]](
    (_   => {case e if tree.isRoot(e) => Map.empty}),
    (out => {case e@Inline() if !isInlineBox(e) => out(e).updated(containingBlock(e),e)})
  )

  val prevInline: Element => Option[Element] = attr {
    case a => prevInlineChain.in(a).get(containingBlock(a))
  }

  val isInlineLevel: Element => Boolean = attr {
    case Display(DisplayInline() | DisplayInlineTable() | DisplayInlineBlock()) => true
    case _ => false
  }

  val isBlockLevel: Element => Boolean = attr {
    case Display(DisplayBlock() | DisplayListItem() | DisplayTable()) => true
    case _ => false
  }

  val relx: Element => Double = attr {
    case n@AnonElem(_:LineBox)        => 0
    case n@Inline() if lineBoxHead(n) => 0

    // case n@Inline() => prevInline(n) map relRight getOrElse 0

    case n@Inline() => prevInline(n) match {
      case Some(p) => relRight(p)
      case None    => 0
    }
    case _ => 0 //TODO
  }

  val rely: Element => Double = attr {
    case n@AnonElem(LineBox(c, None   )) => 0
    case n@AnonElem(LineBox(c, Some(p))) => relBottom(AnonElem(p))
    case Inline() => 0
    case _        => 0 //TODO
  }

  val width: Element => Double = attr {
    case AnonElem(LineBox(c,_)) => width(c)
    case n@InlineBox()          => chren(n).map(absright).max - absx(n)
    case n@Both(HasSpecifiedWidth(w), tree.parent(par)) => min(w, width(containingBlock(n)))
    case HasSpecifiedWidth(w)   => w
    case tree.parent(par)       => width(par)
    case _                      => windowWidth
  }

  val height: Element => Double = attr {
    case AnonElem(lb:LineBox)    => max0(lineMembers(lb).map(relBottom))
    case n@InlineBox()           => chren(n).map(absbottom).max - absy(n)
    case n@HasSpecifiedHeight(h) => max(h, chren(n).map(height).sum)
    case n                       => chren(n).map(height).sum
  }

  val relRight:  Element => Double = fsum(relx,width)
  val relBottom: Element => Double = fsum(rely,height)

  // convert relative to absolute positions (sometimes) trivially
  val absx: Element => Double = attr {
    case n@AnonElem(LineBox(c,_)) => absx(c)+relx(n)
    case n@InlineBox()            => chren(n).map(absx).min
    case n@Inline()               => absx(AnonElem(lineBox(n))) + relx(n)
    case _                        => 0 //TODO
    // case n @ tree.parent(par)     => absy(containingBlock(n)) + relx(n)
    // case n                        => relx(n)
  }

  val absy: Element => Double = attr {
    case n@AnonElem(LineBox(c,_)) => absy(c) + rely(n)
    case n@InlineBox()            => chren(n).map(absy).min
    case n@Inline()               => absy(AnonElem(lineBox(n))) + rely(n)
    case _                        => 0 //TODO
    // case n@tree.parent(par)       => absy(containingBlock(n)) + rely(n)
    // case n                        => rely(n)
  }

  val colorToRGB: PropDef => Color = attr {
    case PropColorDef(c) => c match {
      case ColorRed()          => new Color(0.1f, 0.0f, 0.0f, 0.25f)
      case ColorGreen()        => new Color(0.0f, 1.0f, 0.5f, 0.25f)
      case ColorBlue()         => new Color(0.0f, 0.0f, 1.0f, 0.25f)
      case ColorYellow()       => new Color(0.0f, 0.0f, 0.0f, 0.25f)
      case ColorOrange()       => new Color(0.0f, 0.0f, 0.0f, 0.25f)
      case ColorBrown()        => new Color(0.0f, 0.0f, 0.0f, 0.25f)
      case ColorBlack()        => new Color(0.0f, 0.0f, 0.0f, 0.25f)
      case ColorWhite()        => new Color(0.0f, 0.0f, 0.0f, 0.25f)
      case ColorTransparent()  => new Color(0.0f, 0.0f, 0.0f, 0.25f)
    }
    case _ => new Color(0.0f, 0.0f, 0.0f, 0.0f)
  }

  val renderColor: Element => Color = attr {
    case AnonElem(lb:LineBox) => new Color(0.96f,  0.98f,  0.15f,  0.25f)
    case n if isInlineBox(n)  => new Color(0.96f,  0.98f,  0.15f,  0.0f)
    case n                    => colorToRGB(propColorDef(n))
    // case HasSpecifiedWidth(_) => new Color(0.0f,   1.0f,   0.5f,   0.25f)
    // case _                    => new Color(0.5f,   0.25f,  0.5f,   0.25f)
  }

  override val isRendered: Element => Boolean = attr {
    case AnonElem(_:LineBox) => true
    case n if isInlineBox(n) => false
    case _                   => true
  }

  val absbottom: Element => Double = attr {
    case n => absy(n) + height(n)
  }

  val absright: Element => Double = attr {
    case n => absx(n) + width(n)
  }

  // specify which attributes will be used for rendering
  val renderX      = absx
  val renderY      = absy
  val renderWidth  = width
  val renderHeight = height

}