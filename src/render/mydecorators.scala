package render

import org.bitbucket.inkytonik.kiama.attribution.Decorators
import org.bitbucket.inkytonik.kiama.relation.Tree
import org.bitbucket.inkytonik.kiama.==>

//// This file was copied from kiama's default Decorators.scala, and modified ////

/**
 * Decorators are higher-order operations that provide common patterns of
 * tree attribution based on simple attributes or functions. A `Tree` must
 * be supplied to give the decorators access to the tree structure.
 */
class MyDecorators[T <: Product, R <: T](tree : => Tree[T, R]) extends Decorators(tree) {


    import org.bitbucket.inkytonik.kiama.attribution.Attribution
    import scala.PartialFunction
    import attribution.{attr, CachedAttribute}


    def downPar[U](default : U)(a : T ==> U) : CachedAttribute[T, U] =
        fromPar[U](down[U](default)(a))

    def downOptPar[U](a : T ==> U) : CachedAttribute[T, Option[U]] =
        fromPar[Option[U]](downOpt[U](a))

    def fromPar[U](fn:CachedAttribute[T,U]) : CachedAttribute[T,U] = attr {
        case tree.parent(p) => fn(p)
    }

    

    // def down[U](default : T => U)(a : T ==> U) : CachedAttribute[T, U] = {

}



















