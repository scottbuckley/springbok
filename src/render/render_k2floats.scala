package render

import java.awt.{ Color, Graphics, Graphics2D, Font, BasicStroke, Rectangle }
import java.awt.geom.Line2D
import java.awt.font._
import java.text._
import javax.swing._
import org.bitbucket.inkytonik.kiama.attribution._
import org.bitbucket.inkytonik.kiama.attribution.Attribution
import org.bitbucket.inkytonik.kiama.rewriting.{ Rewriter, Strategy }
import scala.collection.generic.CanBuildFrom
import scala.language.higherKinds
import scala.collection.mutable.Set
import org.bitbucket.inkytonik.dsprofile.Events.{ wrap }
import scala.math.{ min, max }

import org.bitbucket.springbok.DocRatsParserSyntax._

class Renderer_K2_Floats(doc:Element) extends PropRenderer(doc) {

  // // import org.bitbucket.inkytonik.kiama.attribution.Decorators
  // val decorators = new MyDecorators(tree)
  // import decorators.{downOptPar, downPar, chain}

  // def b(partialBoolean: PartialFunction[Any,Boolean]): Any => Boolean = {
  //   i => partialBoolean.applyOrElse(i, ((_:Any)=>false))
  // }

  // //            _                  _
  // //   _____  _| |_ _ __ __ _  ___| |_ ___  _ __ ___
  // //  / _ \ \/ / __| '__/ _` |/ __| __/ _ \| '__/ __|
  // // |  __/>  <| |_| | | (_| | (__| || (_) | |  \__ \
  // //  \___/_/\_\\__|_|  \__,_|\___|\__\___/|_|  |___/


  // // Both always passes, but has two inputs, so its use will only match when the sub-
  // // patterns both match.
  // object Both {
  //   def unapply(x: Any): Option[(Any, Any)] =
  //     Some((x, x))
  // }

  // object AnchoredCB {
  //   def unapply(cb: ContainingBlock): Option[Element] = {
  //     cb match {
  //       case ElementContainingBlock(anchor) => Some(anchor)
  //       case _ => None
  //     }
  //   }
  // }

  // //        _   _        _ _           _
  // //   __ _| |_| |_ _ __(_) |__  _   _| |_ ___  ___
  // //  / _` | __| __| '__| | '_ \| | | | __/ _ \/ __|
  // // | (_| | |_| |_| |  | | |_) | |_| | ||  __/\__ \
  // //  \__,_|\__|\__|_|  |_|_.__/ \__,_|\__\___||___/

  // //children
  // val children: Element => Vector[Element] = attr {
  //   case e:Elem => e.optElements
  //   case _      => Vector()
  // }



  // // containing block stuff
  // val cbtop: ContainingBlock => Double = attr {
  //   case InitialContainingBlock() => 0
  //   case AnchoredCB(anchor)       => absy(anchor)
  // }

  // val cbleft: ContainingBlock => Double = attr {
  //   case InitialContainingBlock() => 0
  //   case AnchoredCB(anchor)       => absx(anchor)
  // }

  // val cbwidth: ContainingBlock => Double = attr {
  //   case InitialContainingBlock() => windowWidth
  //   case AnchoredCB(anchor)       => width(anchor)
  // }

  // val cbheight: ContainingBlock => Double = attr {
  //   case InitialContainingBlock() => windowHeight
  //   case AnchoredCB(anchor)       => height(anchor)
  // }

  // // 10.1 Definition of "containing block"

  // // The position and size of an element's box(es) are sometimes calculated relative to a certain rectangle, called the containing block of the element. The containing block of an element is defined as follows:

  // // The containing block in which the root element lives is a rectangle called the initial containing block. For continuous media, it has the dimensions of the viewport and is anchored at the canvas origin; it is the page area for paged media. The 'direction' property of the initial containing block is the same as for the root element.
  // // For other elements, if the element's position is 'relative' or 'static', the containing block is formed by the content edge of the nearest block container ancestor box.
  // // If the element has 'position: fixed', the containing block is established by the viewport in the case of continuous media or the page area in the case of paged media.
  // // If the element has 'position: absolute', the containing block is established by the nearest ancestor with a 'position' of 'absolute', 'relative' or 'fixed', in the following way:
  // //   In the case that the ancestor is an inline element, the containing block is the bounding box around the padding boxes of the first and the last inline boxes generated for that element. In CSS 2.1, if the inline element is split across multiple lines, the containing block is undefined.
  // //   Otherwise, the containing block is formed by the padding edge of the ancestor.
  // //   If there is no such ancestor, the containing block is the initial containing block.

  // val isInFlow: Element => Boolean = attr {
  //   //TODO
  //   case _ => false
  // }

  // val onNewLine: Element => Boolean = attr {
  //   //TODO
  //   case _ => false
  // }

  // val prevInFlow: Element => Option[Element] = attr {
  //   case a => pifChain(a).get(containingBlock(a))
  // }

  // val pifChain = chain[Map[ContainingBlock,Element]](
  //   (_   => {case e if tree.isRoot(e) => Map.empty}),
  //   (out => {case e:Element if isInFlow(e) => out(e).updated(containingBlock(e),e)})
  // )


  // val abChain = chain[Seq[AnonElem]](
  //   (_   => {case e if tree.isRoot(e) => Seq(new AnonElem(InitialContainingBlock()))}),
  //   (out => {case e if onNewLine(e)  => out(e) :+ AnonElem(containingBlock(e))})
  // )



  // val containingBlock: Element => ContainingBlock = attr {
  //   case tree.parent() /* no parent */                       => InitialContainingBlock()
  //   case n @ Position(PositionRelative() | PositionStatic()) => nearestBlockContainer(n)
  //   case Position(PositionFixed())                           => InitialContainingBlock()
  //   case n @ Position(PositionAbsolute())                    => nearestARFBlock(n)
  // }

  // val nearestARFAncestor: Element => Option[Element] =
  //   downOptPar[Element] {
  //     case n @ Position(PositionAbsolute() | PositionRelative() | PositionFixed()) => n
  //   }

  // val nearestARFBlock: Element => ContainingBlock = attr {
  //   case n => nearestARFAncestor(n) match {
  //     case None                        => InitialContainingBlock()
  //     case Some(a) if isInlineLevel(a) => InlineBoundingBlock(a) // this is not yet implemented. it will be tricky.
  //     case Some(a)                     => ContainingBlock(a)
  //   }
  // }

  // // todo: figure out for sure whether we should be able to return the currnent node (and therefore discar the Par)
  // val nearestBlockContainer: Element => ContainingBlock =
  //   downPar[ContainingBlock](InitialContainingBlock()) {
  //     case n if isBlockContainer(n) => ElementContainingBlock(n) // create a new (?) containing block from this element
  //   }

  // // A block container box either contains only block-level boxes or establishes
  // // an inline formatting context and thus contains only inline-level boxes.
  // // TODO: does 'contain' only refer to direct children?
  // val createsBlockFormattingContext: Element => Boolean = attr {
  //   case n if children(n).isEmpty() => false

  //   // case n if !isBlockContainer(n) => false
  // }

  // // § 9.2.1: Except for table boxes, which are described in a later chapter, and
  // // replaced elements, a block-level box is also a block container box. [...]
  // // Not all block container boxes are block-level boxes: non-replaced inline blocks and
  // // non-replaced table cells are block containers but not block-level boxes.
  // val isBlockContainer: Element => Boolean = attr {
  //   case n if isBlockLevel(n) && !isTableBox(n) => true
  //   case n@Display(DisplayInlineBlock() | DisplayTableCell()) if !isReplaced(n) => true
  //   case _ => false
  // }

  // def b(f : PartialFunction[Element,Boolean]): Function[Element,Boolean] = {
  //   (v:Element) => {
  //     f.applyOrElse(v, ((_:Element)=>false))
  //   }
  // }

  // val isBlockBox: Element => Boolean = attr b {
  //   case n if isBlockLevel(n) && isBlockContainer(n) => true
  //   // case _ => false
  // }

  // val isInlineLevel: Element => Boolean = attr b {
  //   case Display(DisplayInline() | DisplayInlineTable() | DisplayInlineBlock()) => true
  //   // case _ => false
  // }

  // val isBlockLevel: Element => Boolean = attr {
  //   case Display(DisplayBlock() | DisplayListItem() | DisplayTable()) => true
  //   case _ => false
  // }

  // val isTableBox: Element => Boolean = attr {
  //   // We don't support tables at the moment
  //   // https://www.w3.org/TR/2011/REC-CSS2-20110607/tables.html#model
  //   case _ => false
  // }

  // val isReplaced: Element => Boolean = attr {
  //   // We don't support replaced element such as images
  //   // https://www.w3.org/TR/2011/REC-CSS2-20110607/conform.html#defs
  //   case _ => false
  // }











  // // x position relative to CONTAINING BLOCK
  // val relx: Element => Double = attr {
  //   case _ => 0.0 //TODO
  // }

  // // y position relative to CONTAINING BLOCK
  // val rely: Element => Double = attr {
  //   case tree.prev(prev) => rely(prev) + height(prev)
  //   case _ => 0.0
  // }

  // val width: Element => Double = attr {
  //   case Both(Width(w), tree.parent(par)) => min(w, width(par))
  //   case Width(w) => w
  //   case tree.parent(par) => width(par)
  //   case _ => windowWidth
  // }

  // val height: Element => Double = attr {
  //   case n @ Height(h) => max(h, chren(n).map(height).sum)
  //   case n => chren(n).map(height).sum
  // }

  // // convert relative to absolute positions trivially
  // val absy: Element => Double = attr {
  //   case n @ tree.parent(par) => absy(par) + rely(n)
  //   case n => rely(n)
  // }
  // val absx: Element => Double = attr {
  //   case n @ tree.parent(par) => absx(par) + relx(n)
  //   case n => relx(n)
  // }

  // val renderColor: Element => Color = attr {
  //   case Width(_) => new Color(0.0f, 1.0f, 0.5f, 0.25f)
  //   case _ => new Color(0.5f, 0.25f, 0.5f, 0.25f)
  // }

  // // specify which attributes will be used for rendering
  // val renderX = absx
  // val renderY = absy
  // val renderWidth = width
  // val renderHeight = height

  val renderColor: Element => Color = attr {
    case _ => new Color(0.0f, 1.0f, 0.5f, 0.25f)
  }

  // specify which attributes will be used for rendering
  val renderX: Element => Double = attr{case _ => 0.0}
  val renderY: Element => Double = attr{case _ => 0.0}
  val renderWidth: Element => Double = attr{case _ => 0.0}
  val renderHeight: Element => Double = attr{case _ => 0.0}

}