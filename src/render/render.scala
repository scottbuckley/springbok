package render

import java.awt.{ Graphics, Graphics2D, Font, BasicStroke, Rectangle, Color }
import java.awt.geom.Line2D
import java.awt.font._
import java.text._
import javax.swing._
import org.bitbucket.inkytonik.kiama.attribution._
import org.bitbucket.inkytonik.kiama.attribution.Attribution
import org.bitbucket.inkytonik.kiama.rewriting.{ Rewriter, Strategy }
import scala.collection.generic.CanBuildFrom
import scala.language.higherKinds
import scala.collection.mutable.Set
import org.bitbucket.inkytonik.dsprofile.Events.{ wrap }

import org.bitbucket.inkytonik.kiama.relation.Tree
import org.bitbucket.springbok.DocRatsParserSyntax._

abstract class Renderer(doc: Element) extends Attribution {

  // these are initialised by init()
  // var doc: Element = null
  var tree: Tree[Element, Element] = new Tree(this.doc, {case _:Element => true; case _ => false})

  var g2: Graphics2D = null
  var windowWidth: Double = 0
  var windowHeight: Double = 0

  def init(g2: Graphics2D, doc: Element): Unit = {

    // initialise doc and tree
    // this.doc = doc
    // this.tree = new Tree(this.doc, {case _:Element => true; case _ => false})

    this.g2 = g2
    windowWidth = g2.getClipBounds().getWidth - 1
    windowHeight = g2.getClipBounds().getHeight - 1
  }

  def render(node: Element): Unit

}

abstract class SimpleRenderer(doc: Element) extends Renderer(doc) {

  // These should be implemented by any concrete renderer
  val renderX:      Element => Double
  val renderY:      Element => Double
  val renderWidth:  Element => Double
  val renderHeight: Element => Double
  
  val chren: Element => Vector[Element] = attr {
    case e:Elem => e.optElements
    case _      => Vector()
  }
  
  val chrenFlat: Element => Vector[Element] = attr {
    case n => chren(n) ++ chren(n).flatMap(chrenFlat)
  }

  override def render(node: Element): Unit = {

    val children = chren(node)

    // the root is just white and fills the whole window.
    if (tree.isRoot(node)) {
      g2.setColor(new Color(1.0f, 1.0f, 1.0f))
      g2.fillRect(0, 0, windowWidth.toInt, windowHeight.toInt)
    } else {

      // pull box dimensions from predetermined attribute names
      val x: Double = renderX(node)
      val y: Double = renderY(node)
      val w: Double = renderWidth(node)
      val h: Double = renderHeight(node)

      g2.setStroke(new BasicStroke())

      // red translucent fill
      g2.setColor(new Color(1.0f, 0.0f, 0.0f, 0.25f))
      g2.fillRect(
        x.toInt, y.toInt, w.toInt, h.toInt
      )

      // black outline
      g2.setColor(new Color(0.0f, 0.0f, 0.0f))
      g2.drawRect(
        x.toInt, y.toInt, w.toInt, h.toInt
      )
    }

    // recursively render children
    children.map(render)
  }
}

abstract class ColoredRenderer(doc:Element) extends SimpleRenderer(doc) {

  val renderColor: Element => Color
  val alsoRender:  Element => Vector[Element] = attr {case _ => Vector()}
  val isRendered:  Element => Boolean         = attr {case _ => true}

  val isRoot: Element => Boolean = attr {
    case _:AnonElem => false
    case n          => tree.isRoot(n)
  }

  override def render(node: Element): Unit = {

    val children = chren(node)
    val extra    = alsoRender(node)

    // the root is just white and fills the whole window.
    if (isRoot(node)) {
      g2.setColor(new Color(1.0f, 1.0f, 1.0f))
      g2.fillRect(0, 0, windowWidth.toInt, windowHeight.toInt)
    } else {

      // pull box dimensions from predetermined attribute names
      val x: Double = renderX(node)
      val y: Double = renderY(node)
      val w: Double = renderWidth(node)
      val h: Double = renderHeight(node)
      val c: Color  = renderColor(node)

      // note that right now we still ask for the positions of elements that
      // are not rendered. everything will still work if we don't though.
      val draw: Boolean = isRendered(node)

      if (draw) {

        g2.setStroke(new BasicStroke())

        // get fill color from attribute
        g2.setColor(c)
        g2.fillRect(
          x.toInt, y.toInt, w.toInt, h.toInt
        )

        // black outline
        g2.setColor(new Color(0.0f, 0.0f, 0.0f))
        g2.drawRect(
          x.toInt, y.toInt, w.toInt, h.toInt
        )
      }
    }

    // render any extra nodes
    extra.map(render) // call render on the results of alsoRender.

    // recursively render children
    children.map(render)
  }
}