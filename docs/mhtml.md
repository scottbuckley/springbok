Functional User Interfaces in Scala
===================================

This library provides a way to write user interfaces which don't require mutable state in use and in fact only use mutable state at the very last phase of interface rendering, the actual drawing of characters and lines on the screen.  The library is built around two languages and a Scala interface; MTML for layout, MSS for style, and Doc translation in scala code for behaviour.

This library has two purposes, to show how GUIs can be built without mutable state and to show how SLE tools can be used for HTML rendering.  It is not intended that this library be a replacement for HTML, or a full implementation of it.  However, we have aimed to include a representative subset of formal HTML behaviour for two reasons; it stengthens the demonstration of SLE tools, and it improves developer familiarity - you can generally expect a facility in this system to work the same as, or very similarly to, the equivalent from the web-stack.

A note on nomanclature; we will use "web-stack" to mean the triumvirate of HTML/CSS/JavaScript, for brevity's sake.

Mini Text Markup Language (MTML)
================================

MTML is somewhat like a subset of the HTML markup language.  It uses a subset of HTML's tags and has semantics very similar to, but not exactly the same as, HTML.  MTML is more stict in it's syntax, most notably, all opening tags must have separate closing tags. In the following we describe which tags MTML defines and notes any important differences from HTML.

  * `mtml`: The root tag.  If left out, its presence is inferred.  I.e the root of all MTML documents is an mtml tag.
  * `<h1>`,`<h2>`, `<h3>`, `<h4`>
  * `<b>`, `<i>` `<em>`, `<strong>`
  * `<a>`
  * `<span>` : A span can only contain text, other span elements or b, i, or a elements.
  * `<div>`
  * `<table>`: A table must contain only a sequence of rows
  * `<row>`: A row must only contain a sequence of table headers or table datas
  * `<td>`
  * `<ol>`: An ol element may only contain li elements
  * `<ul>`: A ul element may only contain li elements
  * `<li>`
  * `<p>`
  * `<button>`: A button tag may only contain text

 Mini Syle Sheets (MSS)
 ----------------------

 MSS is somewhat like a subset of CSS.  It uses a subset of CSS's selectors and properties and has semantics very similar to, but not exactly the same as, CSS. An MSS file is a sequence of sytle definitions where each style definition is

 ~~~~
 selector {
   property: value;
   property: value
 }
 ~~~~

 The semi-colon is a separator, so can't be on the last line of a style definition.  In the following I describe the selector syntax and the properties avaiable

 Selectors:

   * `string`: Selects tags of this string
   * `.string`: Selects nodes with this id
   * `#string`: Selects nodes with this class
   * `'string`: Selects text node with this text
   * `selector1 selector2`: Selects nodes which match `selector2` which are inside (at any depth) nodes which match `selector1`
   * `selector1 > selector2`: Selects nodes which match `selector2` which are immediate children of nodes which match `selector1`
   * `selector1, selector2`: Selects nodes which match `selector1` or which match `selector2`   

Properties:
  * `width`: a double
  * `height`: a double
  * `font`: `dialog`, `dialoginput`, `monospaced`, `serif` or `sansserif` - inherited
  * `font-style`: `plain`, `bold`, `italic`, `bolditalic` - inherited
  * `font-size`: an integer (points) - inherited
  * `padding-top`: a double
  * `padding-bottom`: a double
  * `padding-left`: a double
  * `padding-right`: a double
  * `margin-top`: a double
  * `margin-bottom`: a double
  * `margin-left`: a double
  * `margin-right`: a double
  * `border-top`: a double
  * `border-bottom`: a double
  * `border-left`: a double
  * `border-right`: a double

MSS styles (sans the selector and braces) can be put in an MTML file be treating the property as an attribute of the node to which you want it applied, for example `<td width=100.0 padding=80.0>`, or can be included in a separate file, which is applied to all MTML files used in the application.  Styles defined in MTML take preference over styles defined in a separate file and finally a system-wide default MSS style sheet is applied, it has lowest precedence.  Thus styles in a MSS file override default styles and styles inline override styles in MSS files and default styles.


Behaviour
---------

The dynamic behaviour of an HTML page is defined with javascript, which is either within the HTML file, or pulled on from an external file.  The dymanic behaviour in this library is defined with scala code, which is stored in a special value called `behaviour`.  This value defines a transformation from a `Doc` with no behviuor, to one with all the event callbacks attached.  Selector syntax and automatic traversals are avaiable to make this job simpler.  The following events are available for binding:

  * `mouse_clicked`
  * `mouse_entered`
  * `mouse_left`

In each case, the event must be bound to a scala function with takes a `Doc` and returns a transformed `Doc`.  The `Doc` passed to the function is always the entire interface, if you wish to isolate the element on which the event has occured, `this` will reference it in the body of the function.  You will rarely transform the `Doc` by hand, you will normally apply the "select and transform" style of programming using the selector syntax above.

Representative Subset
=====================

Notice that the above langauages includes the fundamental parts of the web-stack:

  * Declarative Languages for layout (MTML) and style (MSS).
  * A separate scripting language.
  * A table layout system _and_ a box layout system.
  * Padding, margins and borders.
  * Selector notation for applying styles, which can then be re-used for dynamic behaviour.
  * An event callback system where events are attached to nodes in a tree which trigger arbitrary code in the scripting language.

Creating a Henge Application
==========================

To create a Henge appliction, one should define the initial interface in an MTML file, define the styles for that interface in a separate MSS file, then create a `main.scala` file to pull the application together.  In the `main.scala` file, you create a class which inherits from `App` and fill in its abstract members.  You must set a value `interface` to the path to your MSS file, a value `style` to the path to your MSS file and a value `behaviour` to the scala function (of type `Doc => Doc`) which will attach event callbacks to the document tree.  The class you have just defined has a `main` method which it inherits from `App` and thus can be run immediately.

Differences Between MTML and HTML
==================================

We attempt to match HTML wherever practicable, but there are many differences, most relating to missing features or extra strictness, some which you might not expect are noted here:
  
  * A `div` must hold either all block elements, or all inline elements, not a mixture of the two.  This restriction is usually enforced inside a HTML engine, but a translation is done on the source to achieve it.  We enforce this restriction right from the source.
  * Any CSS property can be an attribute of a tag, so you don't need a separate `style` attribute.
  * Only CSS properties plus `class`, `href`, `src` and `id` can be attributes of a tag.
  * If there are multiple attibutes in a tag they must be separated by commas.